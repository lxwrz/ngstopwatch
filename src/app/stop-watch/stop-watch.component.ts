import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stop-watch',
  templateUrl: './stop-watch.component.html',
  styleUrls: ['./stop-watch.component.css']
})
export class StopWatchComponent implements OnInit {
  public _hours: number = 0;
  public _minutes: number = 0;
  public _seconds: number = 0;
  public _millis: number = 0;
  public running = false;
  lapsedMilis: number = 0;
  timer: any;

  public get hours() {
    return this._hours < 10 ? "0" + this._hours : this._hours;
  }

  public get minutes() {
    return this._minutes < 10 ? "0" + this._minutes : this._minutes;
  }

  public get seconds() {
    return this._seconds < 10 ? "0" + this._seconds : this._seconds;
  }

  public get millis() {
    return this._millis < 10 ? "0" + this._millis : this._millis;
  }

  constructor() { }

  ngOnInit(): void {
  }

  toggle(): void {
    if (this.running) {
      this.stop();
    } else {
      this.start();
    }
  }

  start(): void {
    this.timer = setInterval(() => {
      this.update();
    }, 10);
    this.running = true;
  }

  private update() {
    this.lapsedMilis += 1;
    this._millis += 1;
    if (this._millis > 99) {
      this._millis = 0;
      this._seconds += 1;
    }

    if (this._seconds > 59) {
      this._seconds = 0;
      this._minutes += 1;
    }

    if (this._minutes > 59) {
      this._minutes = 0;
      this._hours += 1;
    }
  }

  stop(): void {
    clearInterval(this.timer);
    this.running = false;
  }

  reset(): void {
    this.stop();
    this.lapsedMilis = 0;
    this._millis = 0;
    this._seconds = 0;
    this._minutes = 0;
  }
}
